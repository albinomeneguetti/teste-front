import React, { useState, useEffect } from "react";
import firebase from "firebase";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";

import ResetPasswordModal from "../../components/resetPasswordModal";
interface ILogin {
  email: string;
  password: string;
}

export default (): JSX.Element => {
  const [login, setLogin] = useState<ILogin>({ email: "", password: "" });
  const [isOpen, setIsOpen] = useState(false);

  const history = useHistory();

  useEffect(() => {
    const user = localStorage.getItem("user");
    if (user) {
      history.push("/dashboard");
    }
  }, [history]);

  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    try {
      const { user } = await firebase
        .auth()
        .signInWithEmailAndPassword(login.email, login.password);
      console.log(user);
      localStorage.setItem(`user`, JSON.stringify(user));
      history.push("/dashboard");
    } catch (error) {
      console.log(error);
      if (error.code === "auth/invalid-email")
        toast("Email está incorreto", {
          position: "top-center",
          type: "error",
        });

      if (error.code === "auth/wrong-password")
        toast("Senha está incorreta", {
          position: "top-center",
          type: "error",
        });

      if (error.code === "auth/too-many-requests")
        toast("Muitas tentativas, favor tentar novamente mais tarde.", {
          position: "top-center",
          type: "error",
        });
    }
  };

  return (
    <div className="bg-indigo-100">
      <div className="grid grid-cols-1 lg:grid-cols-2">
        <div className="bg-blue-600 lg:min-h-screen lg:flex lg:items-center p-8 sm:p-12">
          <div className="flex-grow">
            <h1 className="text-white text-center text-2xl sm:text-5xl mb-2">
              Seja bem vindo(a)
            </h1>
            <p className="text-center text-gray-200 sm:text-lg">
              Faça seu login para começar
            </p>
          </div>
        </div>
        {/* Card-Login */}
        <div className="lg:min-h-screen lg:flex lg:items-center p-12 lg:p-24 xl:p-48">
          <form
            className="flex-grow bg-white shadow-md rounded-md border border-gray-300 p-8"
            onSubmit={onSubmit}
          >
            <div className="sm:flex sm:items-center mb-2">
              <label htmlFor="username" className=" mr-3 text-gray-700">
                Email
              </label>
              <input
                type="text"
                className="flex-1 w-full text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-3"
                placeholder="Email"
                id="username"
                style={{ marginLeft: 2 }}
                onChange={(event) => {
                  setLogin({
                    email: event.currentTarget.value,
                    password: login.password,
                  });
                }}
                value={login.email}
              />
            </div>
            <div className="sm:flex sm:items-center mb-2">
              <label
                htmlFor="password"
                className="flex-shrink-0 mr-2 text-gray-700"
              >
                Senha
              </label>
              <input
                type="password"
                className="flex-1 w-full text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-3"
                placeholder="Senha"
                id="password"
                onChange={(event) => {
                  setLogin({
                    password: event.currentTarget.value,
                    email: login.email,
                  });
                }}
                value={login.password}
                style={{ marginLeft: 2 }}
              />
            </div>
            <div className="sm:flex sm:items-center justify-between mt-4">
              <button
                className="bg-teal-300 rounded-md px-4 py-1 text-gray-700 focus:outline-none"
                type="submit"
              >
                Login
              </button>
              <button
                className="text-gray-600 focus:outline-none"
                type="button"
                onClick={() => {
                  setIsOpen(true);
                }}
              >
                Recuperar senha
              </button>
            </div>
          </form>
        </div>
      </div>
      <ResetPasswordModal isOpen={isOpen} setIsOpen={setIsOpen} />
    </div>
  );
};
