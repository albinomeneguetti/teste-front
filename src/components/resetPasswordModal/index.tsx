import React, { useState } from "react";
import { toast } from "react-toastify";
import { auth } from "firebase";

interface IExcludeModal {
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

export default ({ isOpen, setIsOpen }: IExcludeModal) => {
  const [email, setEmail] = useState("");

  const sendEmail = async () => {
    try {
      const response = await auth().sendPasswordResetEmail(email);
      console.log(response);
      setIsOpen(false);
    } catch (error) {
      toast("Não pode enviar o email", {
        position: "top-center",
        type: "error",
      });
    }
  };

  if (isOpen) {
    return (
      <>
        <div className="fixed z-10 inset-0 overflow-y-auto">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div className="fixed inset-0 transition-opacity">
              <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <span className="hidden sm:inline-block sm:align-middle sm:h-screen"></span>
            &#8203;
            <div
              className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-headline"
            >
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="sm:flex sm:items-start">
                  <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left w-full">
                    <h3
                      className="text-lg leading-6 font-medium text-gray-900"
                      id="modal-headline"
                    >
                      Redefinir senha
                    </h3>
                    <div className="mt-2">
                      <div className="text-sm leading-5 text-gray-500">
                        <form
                          onSubmit={(event) => {
                            event.preventDefault();
                          }}
                        >
                          <div className="sm:flex sm:items-center mb-2">
                            <label className="z-10" htmlFor="email">
                              Email
                            </label>
                            <input
                              type="email"
                              className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1"
                              id="email"
                              style={{ marginLeft: 5 }}
                              onChange={(event) => {
                                setEmail(event.currentTarget.value);
                              }}
                              value={email}
                            />
                          </div>
                          <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                            <span className="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                              <button
                                type="button"
                                className="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-blue-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-blue-500 focus:outline-none focus:border-blue-700 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                                onClick={sendEmail}
                              >
                                Enviar email
                              </button>
                            </span>
                            <span className="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                              <button
                                type="button"
                                className="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                                onClick={() => {
                                  setIsOpen(false);
                                }}
                              >
                                Cancelar
                              </button>
                            </span>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
  return <></>;
};
