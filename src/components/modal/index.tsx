import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";

import api from "../../services/api";
import { cpfMask, cepMask, phoneMask } from "./mask";
import "./index.css";

export interface IUserModal {
  id: string;
  name: string;
  email: string;
  cpf: number;
  phone: string;
  location: IAddressModal;
}

export interface IAddressModal {
  number?: string;
  street: string;
  cep?: string;
  state: string;
  city: string;
  neighborhood: string;
  complement?: string;
}

export type CreateOrEdit = "create" | "edit";

interface IModal {
  isOpen: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  createOrEdit: CreateOrEdit;
  parentUser?: IUserModal;
  setParentUser?: any;
  users?: any;
  setUsers?: any;
}

export default ({
  isOpen,
  createOrEdit,
  setOpen,
  setParentUser,
  parentUser,
  users,
  setUsers,
}: IModal) => {
  const [user, setUser] = useState<IUserModal>({} as IUserModal);
  const [search, setSearch] = useState(false);

  useEffect(() => {
    if (createOrEdit === "edit") {
      console.log(parentUser);
      if (parentUser) {
        setUser({
          id: parentUser.id,
          name: parentUser.name,
          cpf: cpfMask(parentUser.cpf),
          email: parentUser.email,
          phone: phoneMask(parentUser.phone),
          location: {
            city: parentUser.location.city,
            state: parentUser.location.state,
            neighborhood: parentUser.location.neighborhood,
            street: parentUser.location.street,
            cep: cepMask(parentUser.location.cep),
            complement: parentUser.location.complement,
            number: parentUser.location.number,
          },
        });
        setSearch(true);
      }
    }
  }, [isOpen]);

  const createUser = async () => {
    if (createOrEdit === "create") {
      try {
        const { data } = await api.post("/users", user);
        console.log(data);
        setUsers([...users, user]);
      } catch (error) {
        toast("Não foi possível criar o usuário.", {
          position: "top-center",
          type: "error",
        });
      }
    } else {
      try {
        const { data } = await api.patch(`/users/${user.id}`, user);
        setParentUser(user);
      } catch (error) {
        toast(`Não foi possível editar o ${user.name}.`, {
          position: "top-center",
          type: "error",
        });
      }
    }
  };

  const buscaCep = async () => {
    try {
      const { data } = await api.get(`/locations/cep/${user.location.cep}`);
      setUser({
        ...user,
        location: {
          ...user.location,
          city: data.city,
          state: data.state,
          street: data.street,
          neighborhood: data.neighborhood,
        },
      });
      setSearch(true);
    } catch (error) {
      toast("Não foi possível achar o CEP digitado.", {
        position: "top-center",
        type: "error",
      });
    }
  };

  if (isOpen) {
    return (
      <>
        <div className="fixed z-10 inset-0 overflow-y-auto">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div className="fixed inset-0 transition-opacity">
              <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <span className="hidden sm:inline-block sm:align-middle sm:h-screen"></span>
            &#8203;
            <div
              className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-headline"
            >
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="sm:flex sm:items-start">
                  <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left w-full">
                    <h3
                      className="text-lg leading-6 font-medium text-gray-900"
                      id="modal-headline"
                    >
                      {createOrEdit === "create"
                        ? "Criar usuário"
                        : "Editar usuário"}
                    </h3>
                    <div className="mt-2 w-full">
                      <form
                        className="w-full"
                        onSubmit={(event) => {
                          event.preventDefault();
                        }}
                      >
                        <div className="sm:flex sm:items-center mb-2">
                          <label className="z-10" htmlFor="name">
                            Nome
                          </label>
                          <input
                            type="text"
                            className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1"
                            id="name"
                            style={{ marginLeft: 5 }}
                            onChange={(event) => {
                              setUser({
                                ...user,
                                name: event.currentTarget.value,
                              });
                            }}
                            value={user.name}
                          />
                        </div>
                        <div className="sm:flex sm:items-center mb-2">
                          <label className="z-10" htmlFor="email">
                            Email
                          </label>
                          <input
                            type="email"
                            className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1"
                            id="email"
                            style={{ marginLeft: 5 }}
                            onChange={(event) => {
                              setUser({
                                ...user,
                                email: event.currentTarget.value,
                              });
                            }}
                            value={user.email}
                          />
                        </div>
                        <div className="sm:flex sm:items-center mb-2">
                          <label className="z-10" htmlFor="cpf">
                            CPF
                          </label>
                          <input
                            type="text"
                            className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1"
                            id="cpf"
                            maxLength={14}
                            style={{ marginLeft: 5 }}
                            onChange={(event) => {
                              setUser({
                                ...user,
                                cpf: cpfMask(event.currentTarget.value),
                              });
                            }}
                            value={user.cpf}
                          />
                        </div>
                        <div className="sm:flex sm:items-center mb-2">
                          <label className="z-10" htmlFor="phone">
                            Telefone
                          </label>
                          <input
                            type="text"
                            className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1"
                            id="phone"
                            style={{ marginLeft: 5 }}
                            onChange={(event) => {
                              setUser({
                                ...user,
                                phone: phoneMask(event.currentTarget.value),
                              });
                            }}
                            value={user.phone}
                          />
                        </div>
                        <hr />
                        <h4 className="mt-4 mb-4">Endereço</h4>
                        <div className="sm:flex sm:items-center mb-2">
                          <label className="z-10" htmlFor="cep">
                            CEP
                          </label>
                          <input
                            type="text"
                            className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1"
                            id="cep"
                            maxLength={9}
                            style={{ marginLeft: 5 }}
                            onChange={(
                              event: React.FormEvent<HTMLInputElement>
                            ) => {
                              setUser({
                                ...user,
                                location: {
                                  ...user.location,
                                  cep: cepMask(event.currentTarget.value),
                                },
                              });
                            }}
                            value={user.location?.cep}
                          />
                          <button
                            className="text-sm rounded-md border border-gray-300 px-3 py-1 bg-white ml-3 leading-6 font-medium text-gray-700"
                            type="button"
                            onClick={buscaCep}
                          >
                            Buscar Endereço
                          </button>
                        </div>
                        {search && (
                          <>
                            <div className="sm:flex sm:items-center mb-2">
                              <label htmlFor="city">Cidade</label>
                              <input
                                type="text"
                                className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1 "
                                id="city"
                                style={{ marginLeft: 5 }}
                                onChange={(event) => {
                                  setUser({
                                    ...user,
                                    location: {
                                      ...user.location,
                                      city: event.currentTarget.value,
                                    },
                                  });
                                }}
                                value={user.location.city}
                              />
                            </div>
                            <div className="sm:flex sm:items-center mb-2">
                              <label htmlFor="state">Estado</label>
                              <input
                                type="text"
                                className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1 "
                                id="state"
                                style={{ marginLeft: 5 }}
                                onChange={(event) => {
                                  setUser({
                                    ...user,
                                    location: {
                                      ...user.location,
                                      state: event.currentTarget.value,
                                    },
                                  });
                                }}
                                value={user.location.state}
                              />
                            </div>
                            <div className="sm:flex sm:items-center mb-2">
                              <label htmlFor="neighborhood">Bairro</label>
                              <input
                                type="text"
                                className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1 "
                                id="neighborhood"
                                style={{ marginLeft: 5 }}
                                onChange={(event) => {
                                  setUser({
                                    ...user,
                                    location: {
                                      ...user.location,
                                      neighborhood: event.currentTarget.value,
                                    },
                                  });
                                }}
                                value={user.location.neighborhood}
                              />
                            </div>
                            <div className="sm:flex sm:items-center mb-2">
                              <label htmlFor="street">Rua</label>
                              <input
                                type="text"
                                className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1 "
                                id="street"
                                style={{ marginLeft: 5 }}
                                onChange={(event) => {
                                  setUser({
                                    ...user,
                                    location: {
                                      ...user.location,
                                      street: event.currentTarget.value,
                                    },
                                  });
                                }}
                                value={user.location.street}
                              />
                            </div>
                            <div className="sm:flex sm:items-center mb-2">
                              <label htmlFor="number">Número</label>
                              <input
                                type="text"
                                className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1"
                                id="number"
                                style={{ marginLeft: 5 }}
                                onChange={(event) => {
                                  setUser({
                                    ...user,
                                    location: {
                                      ...user.location,
                                      number: event.currentTarget.value,
                                    },
                                  });
                                }}
                                value={user.location?.number}
                              />
                            </div>
                            <div className="sm:flex sm:items-center mb-2">
                              <label htmlFor="complement">Complemento</label>
                              <input
                                type="text"
                                className="flex-1 text-gray-600 bg-gray-200 rounded-md outline-none py-1 px-1"
                                id="complement"
                                style={{ marginLeft: 5 }}
                                onChange={(event) => {
                                  setUser({
                                    ...user,
                                    location: {
                                      ...user.location,
                                      complement: event.currentTarget.value,
                                    },
                                  });
                                }}
                                value={user.location?.complement}
                              />
                            </div>
                          </>
                        )}
                        <div className="py-3 sm:flex sm:flex-row-reverse">
                          <span className="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                            <button
                              type="submit"
                              className="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-teal-500 text-base leading-6 font-medium text-white shadow-sm hover:bg-teal-400 focus:outline-none transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                              onClick={createUser}
                            >
                              Criar
                            </button>
                          </span>
                          <span className="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                            <button
                              type="button"
                              className="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                              onClick={() => {
                                setUser({
                                  location: { city: "" },
                                } as IUserModal);
                                setSearch(false);
                                setOpen(false);
                              }}
                            >
                              Cancelar
                            </button>
                          </span>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
  return <></>;
};
